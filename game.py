import time

class Player():
    def __init__(self):
        self.score = 0

class Game():
    def __init__(self, players, goal, display):
        if not isinstance(players, int) or players <= 0:
            raise ValueError("players must be a positive integer")
        if not isinstance(goal, int) or goal <= 0:
            raise ValueError("goal must be a positive integer")

        self._players = []
        for _ in range(players):
            self._players.append(Player())

        self._goal = goal
        self._display = display

        self.victor = None

        self.drawField()

    # Draws the basic structure of the game field
    def drawField(self, bars=False):
        self._display.fill(0)

        # Draw centre divider
        self._display.vline(int(self._display.width/2-1), 0, self._display.height, 1)
        # self._display.vline(int(self._display.width/2), 0, self._display.height, 1)

        if bars:
            self.drawBars()
        else:
            self._display.show()

    def _getBarLen(self, player):
        score = self._players[player].score
        return int(score / self._goal * (self._display.width / 2 - 1))

    # Draws the players bars
    def drawBars(self):
        # Left
        self._display.fill_rect(
            0,
            0,
            self._getBarLen(0),
            self._display.height,
            1,
        )

        # Right
        bar = self._getBarLen(1)
        self._display.fill_rect(
            self._display.width - 1 - bar,
            0,
            bar,
            self._display.height,
            1,
        )

        self._display.show()

    # Display a countdown - designed to be used for start values < 10
    def countdown(self, start):
        count = start
        while count > 0:
            self._display.fill_rect(
                int(self._display.width / 2) - 5,
                int(self._display.height / 2) - 6,
                12,
                12,
                0,
            )
            self._display.text(
                str(count),
                int(self._display.width / 2) - 4,
                int(self._display.height / 2) - 4,
                1,
            )
            self._display.show()

            time.sleep(1)
            count -= 1

        self._display.fill_rect(
            int(self._display.width / 2) - 6,
            int(self._display.height / 2) - 6,
            12,
            12,
            0,
        )
        self._display.text(
            "GO!",
            int(self._display.width / 2) - 13,
            int(self._display.height / 2) - 4,
            1,
        )
        self._display.show()

    def addScore(self, player):
        if not isinstance(player, int) or player < 0:
            raise ValueError("player must be a non-negative integer")
        if player > len(self._players):
            raise ValueError("invalid player")

        self._players[player].score += 1

    def checkVictory(self):
        for idx, player in enumerate(self._players):
            if player.score >= self._goal:
                self.victor = idx
                return True

        # No victory detected
        return False

    def drawVictory(self):
        if self.victor == 0:
            # Left won
            self._display.text("YOU", 4, 13, 0)
            self._display.text("WIN", 4, 23, 0)

        elif self.victor == 1:
            # Right won
            self._display.text("YOU", 35, 13, 0)
            self._display.text("WIN", 35, 23, 0)

        self._display.show()
