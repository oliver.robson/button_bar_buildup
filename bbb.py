from machine import I2C, Pin, Timer
import d1_mini
import ssd1306
import time

from menu import Menu
from game import Game

# Configuration
DEBOUNCE = 1000  # us
UPDATE_PERIOD = 1000 # us

class Button():
    def __init__(self, pin, debounce):
        self.pin = pin
        self.pressed_flag = False
        self.last = time.ticks_us()
        self._DEBOUNCE = debounce

    def cb(self):
        # debounce
        if (
            not self.pin.value()
            and time.ticks_diff(time.ticks_us(), self.last) > self._DEBOUNCE
        ):
            self.pressed_flag = True

        self.last = time.ticks_us()

def pinCallback(pin):
    for button in BUTTONS:
        if str(pin) == str(BUTTONS[button].pin):
            BUTTONS[button].cb()
            return

def clearButtonFlags():
    for button in BUTTONS:
        BUTTONS[button].pressed_flag = False

# Hardware setup
BUTTONS = {
    "left": Button(Pin(d1_mini.D3, Pin.IN, Pin.PULL_UP), DEBOUNCE),
    "right": Button(Pin(d1_mini.D7, Pin.IN, Pin.PULL_UP), DEBOUNCE),
}
for button in BUTTONS:
    BUTTONS[button].pin.irq(pinCallback)

i2c = I2C(-1, scl=Pin(d1_mini.SCL), sda=Pin(d1_mini.SDA))
WIDTH, HEIGHT = 64, 48
oled = ssd1306.SSD1306_I2C(64, 48, i2c)

while True:
    clearButtonFlags()

    # Main menu
    menu = Menu(oled)
    while not BUTTONS["right"].pressed_flag:
        if BUTTONS["left"].pressed_flag:
            menu.next()
            BUTTONS["left"].pressed_flag = False

        time.sleep_us(UPDATE_PERIOD)

    # Starting game
    game = Game(2, menu.getGoal(), oled)
    game.countdown(3)

    clearButtonFlags()

    # Play the game!
    go_clear_timer = Timer(-1)
    go_clear_timer.init(
        period=1000,
        mode=Timer.ONE_SHOT,
        callback=lambda t: game.drawField(bars=True)
    )

    while not game.checkVictory():
        if BUTTONS["left"].pressed_flag:
            game.addScore(0)
            BUTTONS["left"].pressed_flag = False

        if BUTTONS["right"].pressed_flag:
            game.addScore(1)
            BUTTONS["right"].pressed_flag = False

        game.drawBars()
        time.sleep_us(UPDATE_PERIOD)

    # Clear timer incase we finished before it did
    go_clear_timer.deinit()

    # Show victory screen
    game.drawVictory()
    time.sleep(5)
