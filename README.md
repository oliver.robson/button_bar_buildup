# Button Bar Buildup

This is the code for a 2 player game, where the goal is to press your button
faster than your opponent in order to build your bar faster and reach the goal!

## Hardware

This code isdesigned to run on an ESP32 running MicroPython, specifically with
the hardware setup of a TTGO Mini 32 running the code, and with an OLED display
shield attached to the Mini 32, and an
[ESP32 Tri-Tray](https://gitlab.com/oliver.robson/esp32-tri-tray) used to also
connect button shields on either side of the ESP32 (ideally with the
right-hand one configured to use D7 instad of D3).

## Software

### Prerequisites

The code requires `d1_mini.py` from the
[MicroPython Workshop repo](https://github.com/mattytrentini/micropython_workshop),
so you will need to grab that separately and put it on your device.

### Deployment

Throw all the `.py` files from this repo, along with `d1_mini.py`, onto your
device and you should be ready to rock, assuming your hardware is configured
as recommended in the Hardware section above!

## Game Instructions

On the main menu, press the left button to cycle through difficulties (number
of presses required to reach the goal), and press the right button when it's
time to start the game.

Once in game and "GO!" has appeared, press your button as fast as you can to
fill the white bar and hit the line in the middle! Do it faster than your
opponent and you will be crowned the victor.
