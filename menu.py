class Difficulty():
    def __init__(self, name, goal):
        if not isinstance(goal, int) or goal <= 0:
            raise ValueError("goal must be a positive integer")

        self.name = str(name)
        self.goal = goal

    def __str__(self):
        return self.name


class Menu():
    _DIFFICULTIES = (
        Difficulty("Easy", 31),  # 64 / 2 - 1
        # Difficulty("Medium", 62),  # 31 * 2 - 3
        Difficulty("Hard", 124),
    )

    def __init__(self, display):
        self._display = display
        self._optionIndex = 0
        self.proceed = False

        self._showMenu()

    def _showMenu(self):
        self._display.fill(0)
        self._display.fill_rect(0, 0, self._display.width, 20, 1)

        self._display.text("Bar", 19, 0, 0)
        self._display.text("Buildup", 4, 10, 0)

        for idx, diff in enumerate(self._DIFFICULTIES):
            self._display.text(str(diff), 10, 21+9*idx, 1)

        self._display.text("<Sel Go>", 0, self._display.height-9, 1)

        self._updateMenu()

    def _updateMenu(self):
        self._display.fill_rect(0, 20, 10, 10*len(self._DIFFICULTIES)-1, 0)
        self._display.text(">", 0, 20+10*self._optionIndex, 1)

        self._display.show()

    def next(self):
        self._optionIndex += 1
        if self._optionIndex >= len(self._DIFFICULTIES):
            self._optionIndex = 0

        self._updateMenu()

    def getGoal(self):
        return self._DIFFICULTIES[self._optionIndex].goal
